<?php get_header(); ?>

<div id="all">  <!-- TODO O CONTEÚDO -->
	<div id="ad"><?php dynamic_sidebar('top_sidebar'); ?></div> <!-- anúncios -->

	<div id="content">  <!-- publicações -->
		
		<div id="l_column">
			<?php if(have_posts()) : while(have_posts()): the_post(); ?>
				<div id="all_post">
					<div id="top">
						<div id="title"> <h1><?php the_title(); ?></h1> </div>
					</div>
					<div id="p_content"> <?php the_content(); ?> </div>
				</div>
			<?php endwhile; ?> 
		<?php endif; ?>
	</div>

	<!-- INÍCIO COLUNA DIREITA -->

	<div id="r_column">
		<div class="nostyle">
			<form action="">
				<div id="divBusca">
					<img src="<?php bloginfo('template_directory'); ?> /assets/images/search.ico" id="btnBusca" alt="Buscar"/>
					<input type="text" name="s" id="txtBusca" placeholder="Buscar..."/>
					<button id="btnBusca" type="submit">Buscar</button>
				</div>
			</form>
			<?php dynamic_sidebar('right_column'); ?>
		</div>
	</div>

</div>
</div>

<?php get_footer(); ?>