<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title><?php bloginfo('name'); echo " | "; bloginfo('description') ?></title>
	<link href="<?php bloginfo('template_url'); echo '\menu.css?'; echo date('mdyhis'); ?>" rel="stylesheet" type="text/css" >
    <link href="<?php bloginfo('template_url'); echo '\style.css?'; echo date('mdyhis'); ?>" rel="stylesheet" type="text/css" >
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <?php if(is_home()): ?>
        <div class="header">
            <div id="banner"><img src="<?php header_image(); ?>"></div>
            <input type="checkbox" id="control-nav" />
            <label for="control-nav" class="control-nav"></label>
            <label for="control-nav" class="control-nav-close"></label>
            <nav id="menu">
                <?php wp_nav_menu(
                    array('theme_location' => 'header-menu')
                ); ?>
          </nav>    
      </div>

      <style>
      .header #banner img{
        /*background-image: url('<?php header_image(); ?>');*/
        background-repeat: no-repeat;
    </style>

    <?php else: ?>
        <div class="header">
            <div id="banner"><img src=""></div>
            <input type="checkbox" id="control-nav" />
            <label for="control-nav" class="control-nav"></label>
            <label for="control-nav" class="control-nav-close"></label>
            <nav id="menu">
                <?php wp_nav_menu(
                    array('theme_location' => 'header-menu')
                ); ?>
            </nav>
        </div>

        <style>
        .header #banner{
            height: 400px;
            background-image: url('<?php the_post_thumbnail_url(); ?>');
            background-repeat: no-repeat;
            -webkit-background-size: cover;  /* Propriedade Cover preenche o elemento inteiro*/
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            /*background-repeat: no-repeat;*/
        }

    </style>
<?php endif; ?>