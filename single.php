<?php get_header(); ?>

<?php if(have_posts()) : while(have_posts()): the_post(); ?>
		<div id="all">  <!-- TODO O CONTEÚDO -->

                <div id="ad"> <?php dynamic_sidebar('top_sidebar'); ?></div> <!-- anúncios -->

                <div id="content">  <!-- publicações -->

                    <div id="l_column">

                       <div id="all_post">

                          <div id="top">

                            <div id="title">
                                <?php $img_top = get_field('imagem_do_titulo'); ?>
                                <?php if($img_top != ''):?>
                                    <div id="img_top"><img src="<?php echo $img_top; ?>"></div>
                                    <?php else: ?>

                                    <?php endif;?>
                                    <h1><a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a></h1>

                                </div> 

                                <div id="date">

                                    <?php
                                    ?>
                                    <div class="dia"><?php the_time('d'); ?></div>
                                    <div class="mes"><?php the_time('M'); ?></div>
                                    <div class="ano"><?php the_time('y'); ?></div>
                                </div>
                            </div>

                            <div id="p_content">
                                <div id="text">
                                    <?php the_content(); ?>
                                    <div id="extras">
                                        <div id="links">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div id="p_footer">
                            <div id="social"><div id="content"> 1. FACEBOOK 2. Instagram </div></div>
                            <div id="tags"><div id="content"><?php the_tags(); ?></div></div>
                            <div id="credits">
                                <div id="content">
                                    <div id="c1">Postado por: </div>
                                    <div id="c2"><?php the_author(); ?></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <!-- INÍCIO COLUNA DIREITA -->

                    <div id="r_column">

                        <div class="nostyle">

                            <form action="">
                                <div id="divBusca">
                                  <img src="<?php bloginfo('template_directory'); ?> /assets/images/search.ico" id="btnBusca" alt="Buscar"/>
                                  <input type="text" name="s" id="txtBusca" placeholder="Buscar..."/>

                                  <button id="btnBusca" type="submit">Buscar</button>
                              </div>
                          </form>
                          <?php dynamic_sidebar('right_column'); ?>
                      </div>
                  </div>

              </div>

          </div>

          <?php 
      endwhile; 
  endif; ?>


<?php get_footer(); ?>