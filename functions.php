<?php
require_once('widgets/mu_widget/mu_widget.php');


//Suporte a imagem destacada

$thumbnail = array(
    'flex-width'    => true,
    'width'         => 1170,
    'flex-height'   => true,
    'height'        => 400,
    'default-image' => '',
);
add_theme_support('post-thumbnails', $thumbnail);

// ativando menu dinamico

function register_my_menus() {

    register_nav_menus(
        array(
            'header-menu' => __('Menu Topo')
        )
    );
}
add_action('init', 'register_my_menus');

function reg_sidebar(){
	register_sidebar( 
		array(
          'name' => 'r_column',
          'id'   => 'right_column',
          'description' => 'Descrição' 

      ));
}
add_action('widgets_init', 'reg_sidebar');


function reg_sidebar_top(){
    register_sidebar( 
        array(
            'name' => 'ad',
            'id'   => 'top_sidebar',
            'description' => 'Descrição' 

        ));
}
add_action('widgets_init', 'reg_sidebar_top');
 

$header = array(
    'flex-width'    => true,
    'width'         => 1170,
    'flex-height'   => true,
    'height'        => 400,
    'default-image' => '',
);
add_theme_support( 'custom-header', $header );


$background = array(
	'default-color'          => '',
	'default-image'          => '',
	'default-repeat'         => 'repeat',
	'default-position-x'     => 'left',
    'default-position-y'     => 'top',
    'default-size'           => 'auto',
    'default-attachment'     => 'scroll',
    'wp-head-callback'       => '_custom_background_cb',
    'admin-head-callback'    => '',
    'admin-preview-callback' => '',
);
add_theme_support( 'custom-background', $background );

/* PAGINAÇÃO WORDPRESS */

