<?php get_header(); ?>
<? require 'functions.php';?>

<div id="all">  <!-- TODO O CONTEÚDO -->
    <?php if(is_home()): ?>

        <div id="ad"><?php dynamic_sidebar('top_sidebar'); ?></div> <!-- anúncios -->

        <div id="content">  <!-- publicações -->

            <?php //get the current page
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 0;
            //pagination fixes prior to loop
            $temp =  $query;
            $query = null;
            //custom loop using WP_Query
            $query = new WP_Query( array( 
                'post_status' => 'publish',
                'orderby' => 'date',
                'order' => 'ASC' 
            ) ); 
            //set our query's pagination to $paged
            $query -> query('post_type=post&posts_per_page=2' . '&paged=' . $paged); ?>

            <div id="l_column">
                <?php if ( $query -> have_posts() ) : while ( $query -> have_posts() ) : $query -> the_post(); ?>
                    <div id="all_post">
                        <div id="top">
                            <div id="title">
                                <?php $img_top = get_field('imagem_do_titulo'); ?>
                                <?php if($img_top != ''):?>
                                    <div id="img_top"><img src="<?php echo $img_top; ?>"></div>
                                    <?php else: ?>

                                    <?php endif;?>

                                    <h1><a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a></h1>
                                </div> 
                                <div id="date">
                                    <div class="dia"><?php the_time('d'); ?></div>
                                    <div class="mes"><?php the_time('M'); ?></div>
                                    <div class="ano"><?php the_time('y'); ?></div>
                                </div>
                            </div>

                            <div id="p_content">
                                <div id="text">
                                    <?php the_content(); ?>
                                    <div id="extras">
                                        <div id="links">
                                        </div>
                                    </div>
                                    <div id="ad"><?php dynamic_sidebar('top_sidebar'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div id="p_footer">
                            <div id="social"><div id="content"> 1. FACEBOOK 2. Instagram </div></div>
                            <div id="tags"><div id="content"><?php the_tags(); ?></div></div>
                            <div id="credits">
                                <div id="content">
                                    <div id="c1">Postado por: </div>
                                    <div id="c2"><?php the_author(); ?></div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile;?>
                <?php endif; ?>
                <div id="pagination">
                    <?php  ?>
                    <?php //pass in the max_num_pages, which is total pages ?>
                    <div class="pagenav">
                        <div class="controln control"><?php next_posts_link('Próxima', $query->max_num_pages) ?></div>
                        <div class="controlp control"><?php previous_posts_link('Anterior', $query->max_num_pages) ?></div>
                    </div>
                </div>
            </div>
            
<?php //reset the following that was set above prior to loop
$query = null; $query = $temp; ?>

<!-- INÍCIO COLUNA DIREITA -->

<div id="r_column">
    <div class="nostyle">
        <form action="">
            <div id="divBusca">
              <img src="<?php bloginfo('template_directory'); ?> /assets/images/search.ico" id="btnBusca" alt="Buscar"/>
              <input type="text" name="s" id="txtBusca" placeholder="Buscar..."/>
              <button id="btnBusca" type="submit">Buscar</button>
          </div>
      </form>
      <?php dynamic_sidebar('right_column'); ?>
  </div>
</div>

</div>

<?php else: ?>
<?php endif; ?>

</div>

<?php get_footer(); ?>